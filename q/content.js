document.getElementById("add-to-cart").addEventListener("click", domChange);  //event listener for add-to-cart button

function domChange(){  //checks for changes in the DOM
    var myElement = $('<div class="modal-content js-modal-content></div>')[0];

    var observer = new MutationObserver(function(mutations) {
       if (document.contains(myElement)) {
            console.log("It's in the DOM!");
            observer.disconnect();
        }else{
            console.log('did not work');
        }
    });

    observer.observe(document, {attributes: false, childList: true, characterData: false, subtree:true});
}
