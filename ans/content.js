console.log("TEST 1");
document.getElementById("add-to-cart").addEventListener("click", domChange);  //event listener for add-to-cart button
console.log("TEST 2");

function domChange(){  //checks for changes in the DOM
    console.log("TEST 0");
    var myElement = $('<div class="modal-content js-modal-content></div>')[0];

    var observer = new MutationObserver(function(mutations) {
       if (document.contains(myElement)) {
            console.log("It's in the DOM!");
            observer.disconnect();
        }else{
            console.log('did not work');
        }
    });

    observer.observe(document, {attributes: false, childList: true, characterData: false, subtree:true});
}
